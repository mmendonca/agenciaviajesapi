<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Cliente;
use App\Entity\ClienteViaje;
use App\Repository\ClienteRepository;
use Doctrine\ORM\EntityManagerInterface;


class ClienteController extends AbstractController

{
    /**
     * @Route("/")
     */
    public function home()
    {
        return new Response();
    }

    /**
     * @Route("/GetClientes", name="GetClientes")
     */
    public function GetClientes(Request $request, ClienteRepository $clienteRepository)
    {
        $Clientes = $clienteRepository->findAll();
        $ListaClientes = [];
        foreach ($Clientes as $cliente) {
            $ListaClientes[] = [
                'id' => $cliente->getId(),
                'cedula' => $cliente->getCedula(),
                'nombre' => $cliente->getnombre(),                
                'telefono' => $cliente->gettelefono(),
                'fecnac' => $cliente->getfecnac()
            ];
        };
        
      /*  $response = new JsonResponse();
        $response->setData([
            'success' => true,
            'data' => $ListaClientes                           
        ]);
        return $response;*/
        
		
		$result= [
            'success' => true,
            'data' => $ListaClientes                           
             ];
        


		$response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        $response->setContent(json_encode($result));
        
        return $response;
		
    }


    /**
     * @Route("/GetCliente", name="GetCliente")
     */
    public function GetCliente(Request $request, ClienteRepository $clienteRepository)
    {
        $cliente = new Cliente();
        $response = new Response();
		$response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
		
        $cedula = $request->get('cedula', null);
        if (empty($cedula)) {
            $result= [
                'success' => false,
                'error' => 'Cedula Requerida',
                'data' => null
            ];
            $response->setContent(json_encode($result));        
            return $response;
        }
        
        $repository = $this->getDoctrine()->getRepository(Cliente::class);
        $cliente = $repository->findOneBy([
            'Cedula' => $cedula
        ]);        

        if (!$cliente) {
            $result= [
                'success' => false,
                'error' => 'Cliente No existe',
                'data' => null
            ];
            $response->setContent(json_encode($result));      
            return $response;
        }

        $result= [
            'success' => true,
            'data' => [
                [
                    'id' => $cliente->getId(),
                    'cedula' => $cliente->getCedula(),
                    'nombre' => $cliente->getNombre(),
                    //'fecnac' => $cliente->getFecNac(),
                    'telefono' => $cliente->getTelefono()
                ]
            ]
        ];
        $response->setContent(json_encode($result));      
        return $response;

		
    }


    /**
     * @Route("/CreateCliente", name="CreateCliente")
     */
    public function CreateCliente(Request $request,EntityManagerInterface $em ) {
        $cliente = new Cliente();
        $response = new Response();
		$response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
		
        $cedula = $request->get('cedula', null);
        if (empty($cedula)) {
            $result= [
                'success' => false,
                'error' => 'Cedula Requerida',
                'data' => null
            ];
            $response->setContent(json_encode($result));      
            return $response;

        }
        $nombre = $request->get('nombre', null);
        $fecnac = $request->get('fecnac', null);
        $telefono = $request->get('telefono', null);
        $cliente->setcedula($cedula);
        $cliente->setNombre($nombre);
       // $cliente->setFecNac($fecnac);
        $cliente->setTelefono($telefono);
        $em->persist($cliente);
        $em->flush();
        $result= [
            'success' => true,
            'data' => [
                [
                    'id' => $cliente->getId(),
                    'cedula' => $cliente->getCedula(),
                    'nombre' => $cliente->getNombre(),
                    //'fecnac' => $cliente->getFecNac(),
                    'telefono' => $cliente->getTelefono()
                ]
            ]
        ];
        $response->setContent(json_encode($result));      
        return $response;

    }

    /**
     * @Route("/UpdateCliente", name="UpdateCliente")
     */
    public function UpdateCliente(Request $request,EntityManagerInterface $em, ClienteRepository $clienteRepository)
    {
        $cliente = new Cliente();
        $response = new Response();
		$response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
		
        $cedula = $request->get('cedula', null);
        if (empty($cedula)) {
            $result= [
                'success' => false,
                'error' => 'Cedula Requerida',
                'data' => null
            ];
            $response->setContent(json_encode($result));      
            return $response;

        }                      

        $cliente = $clienteRepository->findOneBy([
            'Cedula' => $cedula
        ]);

        if (!$cliente) {
            $result= [
                'success' => false,
                'error' => 'Cliente No existe',
                'data' => null
            ];
            $response->setContent(json_encode($result));      
            return $response;

        }
        
        $nombre = $request->get('nombre', null);
        if (!empty($nombre)) {
            $cliente->setNombre($nombre);
        }        
        $fecnac = $request->get('fecnac', null);
        if (!empty($fecnac)) {
            $cliente->setFecNac($fecnac);
        }        
        $telefono = $request->get('telefono', null);
        if (!empty($telefono)) {
            $cliente->setTelefono($telefono);
        }
        $em->persist($cliente);
        $em->flush();

        $result= [
            'success' => true,
            'data' => [
                [
                    'id' => $cliente->getId(),
                    'cedula' => $cliente->getCedula(),
                    'nombre' => $cliente->getNombre(),
                    //'fecnac' => $cliente->getFecNac(),
                    'telefono' => $cliente->getTelefono()
                ]
            ]
        ];
        $response->setContent(json_encode($result));      
        return $response;

    }

    /**
     * @Route("/DeleteCliente", name="DeleteCliente")
     */
    public function DeleteCliente(Request $request,EntityManagerInterface $em, ClienteRepository $clienteRepository)
    {
        $cliente = new Cliente();
        $response = new Response();
		$response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
		
        $cedula = $request->get('cedula', null);
        if (empty($cedula)) {
            $result= [
                'success' => false,
                'error' => 'Cedula Requerida',
                'data' => null
            ];
            $response->setContent(json_encode($result));      
            return $response;

        }
        
        $cliente = $clienteRepository->findOneBy([
            'Cedula' => $cedula
        ]);

        if (!$cliente) {
            $result= [
                'success' => false,
                'error' => 'Cliente No existe',
                'data' => null
            ];
            $response->setContent(json_encode($result));      
            return $response;

        }

        $em->remove($cliente);
        $em->flush();
        
        $result= [
            'success' => true,
            'data' => [
                [
                    'id' => $cliente->getId(),
                    'cedula' => $cliente->getCedula(),
                    'nombre' => $cliente->getNombre(),
                    //'fecnac' => $cliente->getFecNac(),
                    'telefono' => $cliente->getTelefono()
                ]
            ]
        ];
        $response->setContent(json_encode($result));      
        return $response;

    }

    /**
     * @Route("/AsignaViaje", name="AsignaViaje")
     */
    public function AsignaViaje(Request $request,EntityManagerInterface $em ) {
        
        $clienteviaje = new ClienteViaje();

        $response = new Response();
		$response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
		
        $cedula = $request->get('cedula', null);
        if (empty($cedula)) {
            $result= [
                'success' => false,
                'error' => 'Cedula Requerida',
                'data' => null
            ];
            $response->setContent(json_encode($result));      
            return $response;

        }
        $codigo = $request->get('codigo', null);
        if (empty($codigo)) {
            $result= [
                'success' => false,
                'error' => 'Codigo Requerido',
                'data' => null
            ];
            $response->setContent(json_encode($result));      
            return $response;

        }
        $nombre = $request->get('nombre', null);
        $fecnac = $request->get('fecnac', null);
        $telefono = $request->get('telefono', null);
        $clienteviaje->setcedula($cedula);
        $clienteviaje->setcodigo($codigo);
        $em->persist($clienteviaje);
        $em->flush();
        $result= [
            'success' => true,
            'data' => [
                [
                    'id' => $clienteviaje->getId(),
                    'cedula' => $clienteviaje->getCedula(),
                    'codigo' => $clienteviaje->getCodigo()                  
                ]
            ]
        ];
        $response->setContent(json_encode($result));      
        return $response;

    }

     /**
     * @Route("/GetViajesClientes", name="GetViajesClientes")
     */
    public function GetViajesClientes(Request $request, ClienteRepository $clienteRepository)
    {        
        $ListaClienteViajes = [];
        $clienteviaje = new ClienteViaje();
        $response = new Response();
		$response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
		
        $cedula = $request->get('cedula', null);
        if (empty($cedula)) {
            $result= [
                'success' => false,
                'error' => 'Cedula Requerida',
                'data' => null
            ];
            $response->setContent(json_encode($result));        
            return $response;
        }
        
        $repository = $this->getDoctrine()->getRepository(ClienteViaje::class);
        $ViajesClientes = $repository->findBy([
            'Cedula' => $cedula
        ]);        

        if (!$ViajesClientes) {
            $result= [
                'success' => false,
                'error' => 'Cliente No Tiene Viajes',
                'data' => null
            ];
            $response->setContent(json_encode($result));      
            return $response;
        }

        $ListaClienteViajes = [];
        foreach ($ViajesClientes as $clienteviaje) {
            $ListaClienteViajes[] = [
                'codigo' => $clienteviaje->getcodigo(),
                'cedula' => $clienteviaje->getCedula(),                
            ];
        };

        $result= [
            'success' => true,
            'data' => $ListaClienteViajes                           
             ];
        
        $response->setContent(json_encode($result));      
        return $response;


		
    }

}