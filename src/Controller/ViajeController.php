<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Viaje;
use App\Repository\ViajeRepository;
use Doctrine\ORM\EntityManagerInterface;


class ViajeController extends AbstractController

{
    /**
     * @Route("/")
     */
    public function home()
    {
        return new Response();
    }

    /**
     * @Route("/GetViajes", name="GetViajes")
     */
    public function GetViajes(Request $request, ViajeRepository $ViajeRepository)
    {
        $Viajes = $ViajeRepository->findAll();
        $ListaViajes = [];
        foreach ($Viajes as $Viaje) {
            $ListaViajes[] = [
                'id' => $Viaje->getId(),
                'codigo' => $Viaje->getCodigo(),
                'plazas' => $Viaje->getPlazas(),                
                'destino' => $Viaje->getDestino(),
                'origen' => $Viaje->getOrigen(),
                'precio' => $Viaje->getPrecio()
            ];
        };
                
		
		$result= [
            'success' => true,
            'data' => $ListaViajes                           
             ];
        


		$response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        $response->setContent(json_encode($result));
        
        return $response;
		
    }


    /**
     * @Route("/GetViaje", name="GetViaje")
     */
    public function GetViaje(Request $request, ViajeRepository $ViajeRepository)
    {
        $Viaje = new Viaje();
        $response = new Response();
		$response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
		
        $codigo = $request->get('codigo', null);
        if (empty($codigo)) {
            $result= [
                'success' => false,
                'error' => 'Codigo Requerido',
                'data' => null
            ];
            $response->setContent(json_encode($result));        
            return $response;
        }
        
        $repository = $this->getDoctrine()->getRepository(Viaje::class);
        $Viaje = $repository->findOneBy([
            'Codigo' => $codigo
        ]);        

        if (!$Viaje) {
            $result= [
                'success' => false,
                'error' => 'Viaje No existe',
                'data' => null
            ];
            $response->setContent(json_encode($result));      
            return $response;
        }

        $result= [
            'success' => true,
            'data' => [
                [
                    'id' => $Viaje->getId(),
                    'codigo' => $Viaje->getCodigo(),
                    'plazas' => $Viaje->getPlazas(),                
                    'destino' => $Viaje->getDestino(),
                    'origen' => $Viaje->getOrigen(),
                    'precio' => $Viaje->getPrecio()
                ]
            ]
        ];
        $response->setContent(json_encode($result));      
        return $response;

		
    }


    /**
     * @Route("/CreateViaje", name="CreateViaje")
     */
    public function CreateViaje(Request $request,EntityManagerInterface $em ) {
        $Viaje = new Viaje();
        $response = new Response();
		$response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
		
        $codigo = $request->get('codigo', null);
        if (empty($codigo)) {
            $result= [
                'success' => false,
                'error' => 'Codigo Requerido',
                'data' => null
            ];
            $response->setContent(json_encode($result));      
            return $response;

        }
        $plazas = $request->get('plazas', null);
        $origen = $request->get('origen', null);
        $destino = $request->get('destino', null);
        $precio = $request->get('precio', null);

        $Viaje->setcodigo($codigo);
        $Viaje->setplazas($plazas);
        $Viaje->setOrigen($origen);
        $Viaje->setDestino($destino);
        $Viaje->setPrecio($precio);
        $em->persist($Viaje);
        $em->flush();
        $result= [
            'success' => true,
            'data' => [
                [
                'id' => $Viaje->getId(),
                'codigo' => $Viaje->getCodigo(),
                'plazas' => $Viaje->getPlazas(),                
                'destino' => $Viaje->getDestino(),
                'origen' => $Viaje->getOrigen(),
                'precio' => $Viaje->getPrecio()
                ]
            ]
        ];
        $response->setContent(json_encode($result));      
        return $response;

    }

    /**
     * @Route("/UpdateViaje", name="UpdateViaje")
     */
    public function UpdateViaje(Request $request,EntityManagerInterface $em, ViajeRepository $ViajeRepository)
    {
        $Viaje = new Viaje();
        $response = new Response();
		$response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
		
        $codigo = $request->get('codigo', null);
        if (empty($codigo)) {
            $result= [
                'success' => false,
                'error' => 'Codigo Requerido',
                'data' => null
            ];
            $response->setContent(json_encode($result));      
            return $response;

        }                      

        $Viaje = $ViajeRepository->findOneBy([
            'Codigo' => $codigo
        ]);

        if (!$Viaje) {
            $result= [
                'success' => false,
                'error' => 'Viaje No existe',
                'data' => null
            ];
            $response->setContent(json_encode($result));      
            return $response;

        }
        
        $plazas= $request->get('plazas', null);
        if (!empty($plazas)) {
            $Viaje->setPlazas($plazas);
        }        
        $origen = $request->get('origen', null);
        if (!empty($origen)) {
            $Viaje->setOrigen($origen);
        }        
        $destino = $request->get('destino', null);
        if (!empty($destino)) {
            $Viaje->setDestino($destino);
        }
        $precio = $request->get('precio', null);
        if (!empty($precio)) {
            $Viaje->setPrecio($precio);
        }
        $em->persist($Viaje);
        $em->flush();

        $result= [
            'success' => true,
            'data' => [
                [
                'id' => $Viaje->getId(),
                'codigo' => $Viaje->getCodigo(),
                'plazas' => $Viaje->getPlazas(),                
                'destino' => $Viaje->getDestino(),
                'origen' => $Viaje->getOrigen(),
                'precio' => $Viaje->getPrecio()
                ]
            ]
        ];
        $response->setContent(json_encode($result));      
        return $response;

    }

    /**
     * @Route("/DeleteViaje", name="DeleteViaje")
     */
    public function DeleteViaje(Request $request,EntityManagerInterface $em, ViajeRepository $ViajeRepository)
    {
        $Viaje = new Viaje();
        $response = new Response();
		$response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
		
        $codigo = $request->get('codigo', null);
        if (empty($codigo)) {
            $result= [
                'success' => false,
                'error' => 'Codigo Requerido',
                'data' => null
            ];
            $response->setContent(json_encode($result));      
            return $response;

        }
        
        $Viaje = $ViajeRepository->findOneBy([
            'Codigo' => $codigo
        ]);

        if (!$Viaje) {
            $result= [
                'success' => false,
                'error' => 'Viaje No existe',
                'data' => null
            ];
            $response->setContent(json_encode($result));      
            return $response;

        }

        $em->remove($Viaje);
        $em->flush();
        
        $result= [
            'success' => true,
            'data' => [
                [
                'id' => $Viaje->getId(),
                'codigo' => $Viaje->getCodigo(),
                'plazas' => $Viaje->getPlazas(),                
                'destino' => $Viaje->getDestino(),
                'origen' => $Viaje->getOrigen(),
                'precio' => $Viaje->getPrecio()
                ]
            ]
        ];
        $response->setContent(json_encode($result));      
        return $response;

    }


}