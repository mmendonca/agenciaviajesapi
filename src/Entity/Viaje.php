<?php

namespace App\Entity;

use App\Repository\ViajeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ViajeRepository::class)
 */
class Viaje
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $Codigo;

    /**
     * @ORM\Column(type="integer")
     */
    private $Plazas;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $Destino;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $Origen;

    /**
     * @ORM\Column(type="float")
     */
    private $Precio;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodigo(): ?string
    {
        return $this->Codigo;
    }

    public function setCodigo(string $Codigo): self
    {
        $this->Codigo = $Codigo;

        return $this;
    }

    public function getPlazas(): ?int
    {
        return $this->Plazas;
    }

    public function setPlazas(int $Plazas): self
    {
        $this->Plazas = $Plazas;

        return $this;
    }

    public function getDestino(): ?string
    {
        return $this->Destino;
    }

    public function setDestino(?string $Destino): self
    {
        $this->Destino = $Destino;

        return $this;
    }

    public function getOrigen(): ?string
    {
        return $this->Origen;
    }

    public function setOrigen(?string $Origen): self
    {
        $this->Origen = $Origen;

        return $this;
    }

    public function getPrecio(): ?float
    {
        return $this->Precio;
    }

    public function setPrecio(float $Precio): self
    {
        $this->Precio = $Precio;

        return $this;
    }
}
