<?php

namespace App\Repository;

use App\Entity\ClienteViaje;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ClienteViaje|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClienteViaje|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClienteViaje[]    findAll()
 * @method ClienteViaje[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClienteViajeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClienteViaje::class);
    }

    // /**
    //  * @return ClienteViaje[] Returns an array of ClienteViaje objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ClienteViaje
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
